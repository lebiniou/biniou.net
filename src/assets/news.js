const NEWS = [
  {
    date: '29/06/2021',
    title: 'Le Biniou 3.60 release',
    description: 'We are excited to announce version 3.60 !<br />This is a major release, focused on better user experience featuring a complete rewrite of the user interface.',
    release: '3.60.0'
  },
  {
    date: '01/06/2021',
    title: 'V4L2 loopback device now available',
    description: 'You can now stream to <a href="https://obsproject.com/">Open broadcaster software</a> ! Wait for version 3.60 to be released, or use the beta version to get it now. Setup instructions <a href="https://wiki.biniou.net/doku.php?id=en:rtmp-streaming">here</a>.'
  },
  {
    date: '01/05/2021',
    title: 'Le Biniou 3.56 released',
    release: '3.56.0'
  },
  {
    date: '30/04/2021',
    title: 'Beta versions available on Flathub',
    description: 'You can now test (at your own risk) experimental/development versions. Installation instructions <a href="https://gitlab.com/lebiniou/lebiniou/-/wikis/Releases">here</a>.'
  },
  {
    date: '27/02/2021',
    title: 'Le Biniou 3.55 released',
    release: '3.55.0'
  },
  {
    date: '14/02/2021',
    title: 'Le Biniou 3.54 "Valentine" release',
    release: '3.54.0'
  },
  {
    date: '20/01/2021',
    title: 'Le Biniou 3.53 released',
    release: '3.53.0'
  },
  {
    date: '07/01/2021',
    title: 'Le Biniou 3.52 released',
    description: 'Now shipped on <strong><a href="https://flathub.org/apps/details/net.biniou.LeBiniou">Flathub</a></strong> ! Available to anyone running a modern GNU/Linux-based distro.',
    release: '3.52.0'
  },
  {
    date: '07/12/2020',
    title: 'Le Biniou 3.51 released',
    release: '3.51'
  },
  {
    date: '31/10/2020',
    title: 'Le Biniou 3.50 "Halloween" release now available',
    description: 'Featuring the long-awaited web interface:',
    screenshot: 'LeBiniou-3.50.png',
    release: '3.50'
  },
  {
    date: '16/10/2020',
    title: 'Le Biniou 3.50 preview',
    description: `Le Biniou 3.50 is hopefully coming soon, featuring a brand new web interface.
    In the meantime, enjoy this video which was broadcasted live on YouTube by <strong><a href="http://manu.kebab.free.fr/">Manu Kebab</a></strong>
    running <strong><a href=""http://io.gnu.linux.free.fr/>IO GNU/Linux</a></strong> !`,
    youtube: 'ZfkcbYPDDJU'
  },
  {
    date: '13/07/2020',
    title: 'Le Biniou 3.43 released',
    release: '3.43'
  },
  {
    date: '12/05/2020',
    title: 'Le Biniou 3.42 released',
    description: 'Featuring 4 new "<stryong>Path</strong>" plugins.',
    release: '3.42'
  },
  {
    date: '30/04/2020',
    title: 'Le Biniou 3.41 released',
    description: 'Featuring a new (but old-school) <strong>Rotozoom</strong> effect by <strong>@oliv3</strong>.',
    youtube: 'IdWNOEuE_CE',
    release: '3.41'
  },
  {
    date: '16/03/2020',
    title: 'Arch Linux package',
    description: 'Thanks to <strong>opale95</strong> you can find an Arch Linux AUR package <a href="https://aur.archlinux.org/packages/lebiniou">here</a>.'
  },
  {
    date: '22/01/2020',
    title: 'Le Biniou 3.40 released',
    description: 'Major release: sequences are now stored in JSON format and plugins can be configured through the GUI.',
    release: '3.40'
  },
  {
    date: '08/12/2019',
    title: 'Le Biniou 3.32 released',
    description: '"Alder Blooming (featuring <strong>Mike Muller</strong>)" - by <strong>Tavasti</strong>',
    youtube: 'ycVrgGtrBmM',
    release: '3.32'
  },
  {
    date: '14/04/2019',
    title: 'Le Biniou 3.31 released',
    description: 'The <strong>Takens</strong> plugin rendering <strong><a href="https://en.wikipedia.org/wiki/John_Sheahan">John Sheahan</a></strong> &ndash; Carolan\'s devotion',
    youtube: '2KEFIAJ6Gyc'
  },
  {
    date: '28/01/2019',
    title: 'Le Biniou 3.30 released (1/2)',
    description: `<strong>Happy birthday !</strong>
    <p>This year celebrates Le Biniou's 25th birthday !</p>
    This video shows the reworked <strong>Takens</strong> effect (4D phase-space projection of the audio signal) with a piece by Eric Marienthal.`,
    youtube: 'RdceMJ1JFHM'
  },
  {
    date: '28/01/2019',
    title: 'Le Biniou 3.30 released (2/2)',
    description: 'Rendering the audio track only',
    youtube: 'qMWkqthGYws'
  },
  {
    date: '10/12/2018',
    title: 'Le Biniou 3.29 released'
  },
  {
    date: '24/11/2018',
    title: 'Le Biniou 3.28 released',
    description: 'Thanks to <strong><a href="https://soundcloud.com/erwan-lerale/tracks">R1L</a></strong> for this small Kraftwerk-like promotional video &#128578;',
    video: 'https://dl.thiscow.fr/sadcomputer.mp4'
  }
]

export default NEWS
