all:
	git push origin beta

.js.minjs:
	@uglifyjs $< > $@

.css.mincss:
	@cleancss $< > $@
